# Recursion examples

This is just a examples repository.
You can find recursion examples.

- fibonacci (1, 2, 3, 5, 8, ...)
- prime-number (1, 2, 3, 5, 7, 11, 13, ...)
- filetree

# Run

- node fibonacci
- node prime-number
- node filetree
