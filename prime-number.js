/*jshint node:true  */
function start() {
  getNextNumber(1);
}

function getNextNumber(currentNumber) {
  setTimeout(function() {
    var prime = true;
    for (var i = 1; i < currentNumber; i++) {
      if (i == 1 ) continue;
      if (currentNumber % i === 0) {
        prime = false;
        break;
      }
    }
    if (prime) {
      console.log(currentNumber);
    }
    getNextNumber(++currentNumber);
  }, 1000);
}

start();
