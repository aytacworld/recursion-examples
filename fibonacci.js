/*jshint node:true*/
function start() {
  getNextNumber(1, 1);
}

function getNextNumber(previousNumber, currentNumber) {
  console.log(currentNumber);
  setTimeout(function() {
    var nextNumber = previousNumber + currentNumber;
    getNextNumber(currentNumber, nextNumber);
  }, 1000);
}

start();
