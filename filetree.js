/*jshint node:true  */
var filetree = {
  name: 'root',
  children: [
    {
      name: '1. ABC',
      children: [
        {
          name: 'a. A'
        },
        {
          name: 'b. B',
          children: [
            {
              name: 'i. hello'
            },
            {
              name: 'ii. world'
            }
          ]
        },
        {
          name: 'c. C'
        }
      ]
    },
    {
      name: '2. DEF',
      children:[
        {
          name: 'a. second Hello'
        },
        {
          name: 'b. second World'
        }
      ]
    }
  ]
};

function start() {
  createFiletree(filetree, '> ');
}

function createFiletree(filetree, level) {
  console.log(level + filetree.name);
  if (filetree.children && filetree.children.length > 0) {
    level = '-' + level;
    for (var i = 0, max = filetree.children.length; i < max; i++) {
      createFiletree(filetree.children[i], level);
    }
  }
}

start();
